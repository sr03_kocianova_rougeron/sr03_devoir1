package server;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Serveur {
    private List<ServerMessageReceptor> list_threads;
    private List<User> list_users;

    public Serveur() {
        this.list_threads = new ArrayList<>();
        this.list_users = new ArrayList<>();
    }

    public void launch(int port) {
        try{
            ServerSocket sock_server = new ServerSocket(port);
            // boucle d'écoute de connexion
            // lancement de thread à la connexion pour le client
            do {
                Socket client = sock_server.accept();
                DataOutputStream out = new DataOutputStream(client.getOutputStream());
                DataInputStream in = new DataInputStream(client.getInputStream());

                // nouvelle connexion : création du User
                String username = in.readUTF();
                System.out.println(username);

                /*while (!this.username_available(username)){ // check if username is already taken
                    out.writeUTF("Username not available");
                    username = in.readUTF();
                }*/
                out.writeUTF("Available");

                User new_client = new User(username,client,out);
                list_users.add(new_client);

                // envoi du message à tous les users pour la nouvelle connexion
                for (User dest : list_users){
                    dest.out.writeUTF(username + " a rejoint la conversation.");
                }

                // creation d'un nouveau thread
                ServerMessageReceptor thread_user = new ServerMessageReceptor(new_client);

                list_threads.add(thread_user);
                thread_user.run();
            } while (list_users.size()>0);

            // close sockets
            for (ServerMessageReceptor thread : list_threads){
                thread.getClient().sock.close();
            }

            System.out.println("___Serveur fermé___");
            System.out.println("___END___");
        } catch (IOException ex) {
            Logger.getLogger(Serveur.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    boolean username_available(String username){
        if (this.list_users.size()==0){
            return true;
        }

        for (User user : this.list_users ){
            if (Objects.equals(user.username, username)){
                return false;
            }
        }
        return true;
    }



}
