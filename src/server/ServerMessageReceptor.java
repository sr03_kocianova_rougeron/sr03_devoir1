package server;

import java.io.DataInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class ServerMessageReceptor extends Thread{
    public static List<User> list_users;

    static {
        list_users = new ArrayList<>();
    }

    private User client;

    public ServerMessageReceptor(User client){
        list_users.add(client);
        this.setClient(client);
    }

    @Override
    public void run(){
        try {
            DataInputStream in = new DataInputStream(this.client.sock.getInputStream());

            while (true){
                // reception du message
                String message = in.readUTF();
                System.out.println(message);
                if (message.equals("exit")) {
                    // envoie à tous les users message de quit
                    break;
                }

                // envoie à tous les clients même l'envoyeur
                for (User dest : list_users){
                    try{
                        dest.out.writeUTF(this.getClient()+" a dit : "+message);
                    } catch (IOException e){
                        e.printStackTrace();
                        System.out.println("erreur envoi user : "+dest.username);
                    }
                }
            }
            System.out.println("connexion terminée avec user : "+ getClient().username);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public User getClient() {
        return client;
    }

    public void setClient(User client) {
        this.client = client;
    }
}
