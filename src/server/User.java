package server;

import java.io.DataOutputStream;
import java.net.Socket;

public class User {
    public String username;
    public Socket sock;
    public DataOutputStream out;

    public User(String username, Socket sock, DataOutputStream out) {
        this.username = username;
        this.sock = sock;
        this.out = out;
    }

}

