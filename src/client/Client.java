package client;

import server.User;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.util.Scanner;

public class Client {
    private User user ;

    public void launch(int port) throws IOException {
        Socket client=new Socket("localhost", port);
        DataOutputStream out = new DataOutputStream(client.getOutputStream());
        DataInputStream in = new DataInputStream(client.getInputStream());

        try{
            System.out.println("Entrez votre pseudo :");
            Scanner sc = new Scanner(System.in);
            String resp_username = sc.next();
            out.writeUTF(resp_username);

            String resp = in.readUTF();
            System.out.println(resp);
            /*while (resp.equals("Username not available")){
                System.out.println("Pseudo non disponible...");
                System.out.println("Entrez un autre pseudo : ");
                resp_username = sc.nextLine();
            }*/

            String first = in.readUTF(); //
            System.out.println(first); // message : xxx a rejoint la conversation.
            System.out.println("_________________\n");
        } catch(IOException e){
            e.printStackTrace();
        }

        ClientMessageReceptor user_thread = new ClientMessageReceptor(user,in,out);
        user_thread.run();

        // boucle d'écriture
        while (true) {
            Scanner sc = new Scanner(System.in);
            String to_send = sc.next();
            out.writeUTF(to_send);

            if (to_send.equals("exit")){
                client.close();
                break;
            }
        }

    }
}
